from django.conf.urls import url
# from django.urls import path
from . import views

app_name = 'auction'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(template_name='index.html')),
    # path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    # path('<int:lot_id>/bet/', views.BetView, name='bet'),
    url(r'^register/$', views.RegisterFormView.as_view(template_name='register.html')),
    url(r'^login/$', views.LoginFormView.as_view(template_name='login.html')),
]
