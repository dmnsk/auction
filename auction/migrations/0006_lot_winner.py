# Generated by Django 2.0.2 on 2018-03-12 14:34

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('auction', '0005_auto_20180312_1809'),
    ]

    operations = [
        migrations.AddField(
            model_name='lot',
            name='winner',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
