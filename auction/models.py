from django.db import models
from django.contrib.auth.models import User


class Lot(models.Model):

    OPEN = 'Open'
    STOP = 'Stop'
    CANCEL = 'Cancel'
    STATUS_CHOICES = (
        (OPEN, 'Открыт'),
        (STOP, 'Завершён'),
        (CANCEL, 'Отменён'),
    )

    name = models.CharField(max_length=50)
    description = models.TextField(max_length=500)
    image = models.ImageField(upload_to="auction/images", blank=True)
    start_date = models.DateTimeField("Lot start time")
    end_date = models.DateTimeField("Lot end time")
    status = models.CharField(max_length=8, choices=STATUS_CHOICES, default='open')
    bets = models


class Bet(models.Model):
    lot = models.ForeignKey(Lot, None)
    user_bet = models.ForeignKey(User, None)
    time = models.DateTimeField("Bet time")
    price = models.IntegerField(default=0)
