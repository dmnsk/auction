from django.views import generic
from django.utils import timezone
from .models import Lot, Bet
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.http import HttpResponseRedirect
from django.views.generic import View
from django import forms


class IndexView(generic.ListView):
    template_name = 'index.html'
    context_object_name = 'latest_lot_list'

    def get_queryset(self):
        """Возвращает лоты аукциона."""
        return Lot.objects.filter(
            start_date__lte=timezone.now()
        ).order_by('-start_date')[:]


class DetailView(generic.DetailView):
    model = Lot
    template_name = 'detail.html'

    def bet_place(request,):
        print("Вы сделали ставку")


class RegisterFormView(FormView):
    form_class = UserCreationForm

    template_name = "register.html"

    success_url = "/auction/index"

    def form_valid(self, form):
        form.save()

        return super(RegisterFormView, self).form_valid(form)


class LoginFormView(FormView):
    form_class = AuthenticationForm

    template_name = "login.html"

    success_url = "/auction/index"

    def form_valid(self, form):
        self.user = form.get_user()

        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)


class LogoutView(View):
    def __get__(self, request):
        logout(request)
        return HttpResponseRedirect("/auction/login")


class BetForm(forms.Form):
    name = forms.CharField()
    message = forms.CharField(widget=forms.Textarea)

    def make_bet(self):
        pass


class BetView(FormView):
    template_name = "detail.html"
    form_class = BetForm
    success_url = "index.html"

    def form_valid(self, form):
        form.post(request="")

        return super().form_valid(form)


class LotBetsView(generic.ListView):
    template_name = 'detail.html'
    context_object_name = 'latest_bet_list'

    def get_queryset(self):
        # Возвращает ставки на лот
        return Bet.objects.filter(
            time__lte=timezone.now()
        ).order_by('-start_date')[:]
